<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\TableController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'home']);

Route::get('/register', [AuthController::class, 'get_register']);

Route::get('/welcome', [AuthController::class, 'welcome']);

Route::get('/table', [TableController::class, 'get_table']);

Route::get('/data-table', [TableController::class, 'get_dataTable']);

Route::post('/register', [AuthController::class, 'post_register']);
