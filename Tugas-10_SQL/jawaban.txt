1. Buat database

create database myshop;


2. Membuat Table di dalam Database

tabel users
CREATE TABLE `users` ( `id` INT PRIMARY KEY AUTO_INCREMENT, `name` VARCHAR(255) NULL, `email` VARCHAR(255) NULL, `password` VARCHAR(255) NULL );

tabel categories
CREATE TABLE `categories` (
  `id` INT PRIMARY KEY AUTO_INCREMENT,
  `name` VARCHAR(255) NULL
);

tabel items 
CREATE TABLE `items` (
  `id` INT PRIMARY KEY AUTO_INCREMENT,
  `name` VARCHAR(255) NULL,
  `description` VARCHAR(255) NULL,
  `price` INT,
  `stock` INT,
  `category_id` INT,
  FOREIGN KEY (category_id) REFERENCES categories(id)
);


3. Memasukkan data pada tabel
tabel users
INSERT INTO users (id, name, email,password)
VALUE (1, "John Doe", "john@doe.com", "john123"),
(2, "Jane Doe", "jane@doe.com", "jenita123");

tabel categories
INSERT INTO categories (id, name)
VALUE (1, "gadget"), 
(2, "cloth"),
(3, "men"),	
(4, "women"),
(5, "branded");

tabel items 
INSERT INTO items (id, name, description, price, stock, category_id)
VALUE (1, "Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1),
(2, "Uniklooh", "baju keren dari brand ternama", 500000 , 50, 2),
(3, "IMHO Watch" ,"Jam tangan anak yang jujur banget", 2000000, 10, 1);


4. Mengambil data dari database
a. mengambil data users kecuali passwordnya
SELECT id,name,email FROM users; 

b.1. mengambil data items
Buatlah sebuah query untuk mendapatkan data item pada table items yang memiliki harga di atas 1000000 (satu juta).
SELECT * FROM items Where price > 1000000;

b.2. Buat sebuah query untuk mengambil data item pada table items yang memiliki name serupa atau mirip (like) dengan kata kunci “uniklo”, “watch”, atau “sang” (pilih salah satu saja).
SELECT * FROM items WHERE name LIKE '%sang%';

c. Menampilkan data items join dengan kategori
SELECT * FROM items INNER JOIN categories ON items.category_id=categories.id;


5. Mengubah data dari database
UPDATE items SET price = 2500000 WHERE id = 1;