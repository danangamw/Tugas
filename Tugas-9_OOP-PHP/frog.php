<?php
require_once('./animal.php');

class Frog extends Animal
{
    public $cold_blooded = 'yes';

    public function jump()
    {
        echo "Jump: Hop Hop" . '<br>';
    }

    public function get_cold_blooded()
    {
        echo "cold blooded: {$this->cold_blooded}, All frogs are cold blooded" . '<br>';
    }
}
