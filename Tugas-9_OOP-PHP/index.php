<?php
require_once('./animal.php');
require_once('./ape.php');
require_once('./frog.php');

$sheep = new Animal("Shaun");
$sheep->get_name();
$sheep->get_legs();
$sheep->get_cold_blooded();

echo "<br>";

$monkey_king = new Ape("Sun Wukong");
$monkey_king->get_name();
$monkey_king->get_legs();
$monkey_king->get_cold_blooded();
$monkey_king->yell();

echo "<br>";

$katak = new Frog("Buduk");
$katak->get_name();
$katak->get_legs();
$katak->get_cold_blooded();
$katak->jump();
